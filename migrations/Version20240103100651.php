<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240103100651 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE commande (id INT AUTO_INCREMENT NOT NULL, date_c DATETIME DEFAULT NULL, date_f DATETIME DEFAULT NULL, num_c VARCHAR(255) DEFAULT NULL, num_f VARCHAR(255) DEFAULT NULL, statut VARCHAR(255) DEFAULT NULL, mode_pay VARCHAR(255) DEFAULT NULL, total_ht DOUBLE PRECISION DEFAULT NULL, total_ht_hors_livraison DOUBLE PRECISION DEFAULT NULL, total_frais_livraison DOUBLE PRECISION DEFAULT NULL, total_ttc DOUBLE PRECISION DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE commande');
    }
}
