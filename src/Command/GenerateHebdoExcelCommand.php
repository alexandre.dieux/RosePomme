<?php

namespace App\Command;

use App\Entity\Commande;
use App\Traits\ExcelTrait;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class GenerateHebdoExcelCommand extends Command
{
    use ExcelTrait;
    private $entityManager;
    private $mailer;

    public function __construct(EntityManagerInterface $entityManager, MailerInterface $mailer)
    {
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;

        parent::__construct();
    }

    protected function configure(): void
    {
        // On set le nom de la commande
        $this->setName('ps:hebdo');
        // On set la description
        $this->setDescription("Permet de générer un excel des commandes hebdo et d'envoyer par mail");
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $dateStart = new \DateTime('today midnight');
        $dateStart->modify('-7 days');
        $dateEnd = new \DateTime('today midnight');

        $datas = $this->generateContent($dateStart, $dateEnd);
        $tabData = $datas["datas"];
        $tabTotalByStatut = $datas["datasByStatut"];

        $start = $dateStart->format('d/m/Y');
        $end = $dateEnd->format('d/m/Y');
        $weekNumber =  $dateStart->format('W');
        $mailObject = 'Rapport hebdomadaire des commandes - du '.$start.' au '.$end;
        $mailBody = '<p>Bonjour,<br />vous trouverez ci-joint le tableau des commandes entre le '.$start.' et le '.$end.'.</p>';
        $fileName = 'RapportHebdomadaireRosePomme-S'.$weekNumber.'.xlsx';

        $this->generateExcelCommande($tabData, $tabTotalByStatut, $mailObject, $mailBody, $fileName, $this->isDebug);

        return 1;
    }

}