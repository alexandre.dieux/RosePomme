<?php

namespace App\Command;

use App\Entity\Commande;
use App\Entity\Customer;
use App\Traits\ExcelTrait;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class GenerateCustomerHebdoExcel extends Command
{
    use ExcelTrait;
    private $entityManager;
    private $mailer;

    public function __construct(EntityManagerInterface $entityManager, MailerInterface $mailer)
    {
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;

        parent::__construct();
    }

    protected function configure(): void
    {
        // On set le nom de la commande
        $this->setName('ps:customerHebdo');
        // On set la description
        $this->setDescription("Permet de générer un excel des mails clients ajouté la semaine précédente");
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $dateStart = new \DateTime('today midnight');
        $dateStart->modify('-7 days');
        $dateEnd = new \DateTime('today midnight');

        $criteria = new Criteria();
        $criteria
            ->andWhere(Criteria::expr()->gte('dateAdd', $dateStart))
            ->andWhere(Criteria::expr()->lte('dateAdd', $dateEnd));

        $clients = $this->entityManager->getRepository(Customer::class)->matching($criteria);

        $datas = $this->formatClient($clients);

        $columns = ["email"];

        $start = $dateStart->format('d/m/Y');
        $end = $dateEnd->format('d/m/Y');
        $weekNumber =  $dateStart->format('W');
        $mailObject = 'Excel Client - du '.$start.' au '.$end;
        $mailBody = '<p>Bonjour,<br />vous trouverez ci-joint le tableau des mails client inscrits sur le site semaine ' . $weekNumber . '</p>';
        $fileName = 'excelClient-S'.$weekNumber.'.xlsx';

        $this->generateExcelGeneric($columns, $datas, $mailObject, $mailBody, $fileName, $this->isDebug);

        return 1;
    }

}