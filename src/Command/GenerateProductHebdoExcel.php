<?php

namespace App\Command;

use App\Entity\Article;
use App\Entity\Commande;
use App\Entity\Customer;
use App\Traits\ExcelTrait;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class GenerateProductHebdoExcel extends Command
{
    use ExcelTrait;
    private $entityManager;
    private $mailer;

    public function __construct(EntityManagerInterface $entityManager, MailerInterface $mailer)
    {
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;

        parent::__construct();
    }

    protected function configure(): void
    {
        // On set le nom de la commande
        $this->setName('ps:productHebdo');
        // On set la description
        $this->setDescription("Permet de générer un excel des CA par produit");
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $dateStart = new \DateTime('today midnight');
        $dateStart->modify('-7 days');
        $dateEnd = new \DateTime('today midnight');

        $criteria = new Criteria();
        $criteria
            ->andWhere(Criteria::expr()->gte('dateAdd', $dateStart))
            ->andWhere(Criteria::expr()->lte('dateAdd', $dateEnd));

        $articles = $this->entityManager->getRepository(Article::class)->matching($criteria);

        $datas = $this->formatArticles($articles);

        $columns = ["Reference", "Article", "Declinaison", "Quantite", "Prix HT"];

        $start = $dateStart->format('d/m/Y');
        $end = $dateEnd->format('d/m/Y');
        $weekNumber =  $dateStart->format('W');
        $mailObject = 'Excel Articles - du '.$start.' au '.$end;
        $mailBody = '<p>Bonjour,<br />vous trouverez ci-joint le tableau des quantités et du CA par produit sur le site semaine ' . $weekNumber . '</p>';
        $fileName = 'excelArticles-S'.$weekNumber.'.xlsx';

        $this->generateExcelGeneric($columns, $datas, $mailObject, $mailBody, $fileName, $this->isDebug);

        return 1;
    }

}