<?php

namespace App\Command;

use App\Entity\Article;
use App\Entity\Commande;
use App\Entity\Customer;
use App\Traits\ExcelTrait;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class GenerateCommandTotalForThisYearExcel extends Command
{
    use ExcelTrait;
    private $entityManager;
    private $mailer;

    public function __construct(EntityManagerInterface $entityManager, MailerInterface $mailer)
    {
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;

        parent::__construct();
    }

    protected function configure(): void
    {
        // On set le nom de la commande
        $this->setName('ps:totalCommand');
        // On set la description
        $this->setDescription("Permet de générer un excel des CA par produit depuis le début de l'année");
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $dateStart = new \DateTime('today midnight');
        $dateStart->modify('first day of january this year');
        $dateEnd = new \DateTime('today midnight');

        $start = $dateStart->format('d/m/Y');
        $end = $dateEnd->format('d/m/Y');
        $yearNumber =  $dateStart->format('Y');
        $mailObject = 'Excel Commandes - depuis le début de l\'année ' . $yearNumber;
        $mailBody = '<p>Bonjour,<br />vous trouverez ci-joint le tableau des commandes depuis le début de l\'année.</p>';
        $fileName = 'excelCommandeTotal-' . $yearNumber . '.xlsx';

        $this->generateExcelTotalCommande($dateStart, $dateEnd, $mailObject, $mailBody, $fileName, $this->isDebug);

        return 1;
    }
}