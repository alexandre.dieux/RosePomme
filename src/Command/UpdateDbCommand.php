<?php

namespace App\Command;

use App\Entity\Article;
use App\Entity\Commande;
use App\Entity\Customer;
use App\Traits\ConfigTrait;
use Cassandra\Custom;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateDbCommand extends Command
{
    use ConfigTrait;

    private EntityManagerInterface $entityManager;


    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        parent::__construct();
    }

    protected function configure(): void
    {
        // On set le nom de la commande
        $this->setName('ps:update');
        // On set la description
        $this->setDescription("Permet de mettre à jour la base de données des commandes");
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        ini_set("memory_limit",-1);
        $pdo = new \PDO('mysql:host='.$this->IMPORTHOST.';dbname='.$this->IMPORTDB, $this->IMPORTUSER, $this->IMPORTPWD);

        // Gestion des clients
        $query = $pdo->query("
                SELECT 
                    distinct(pc.email) as email,
                    pa.phone,
                    pa.phone_mobile,
                    pa.address1 as address1,
                    pa.address2 as address2,
                    pa.postcode as zip,
                    pa.city as city,
                    pcl.name as country,
                    pc.id_customer as id_customer,
                    pc.firstname as firstname,
                    pc.lastname as lastname, 
                    pc.date_add as date_add, 
                    pc.date_upd as date_upd
                FROM ps_orders po
                join ps_customer pc on pc.id_customer = po.id_customer
                join ps_address pa on pc.id_customer = pa.id_customer and pa.address1 != 'address1' and pa.postcode is not null
                join ps_country_lang pcl on pcl.id_country = pa.id_country and pcl.id_lang = 1
                where po.id_customer != 0
        ");
        $resultatsClientPS = $query->fetchAll();

        $clients = [];

        foreach ($resultatsClientPS as $cli)
        {
            $existingClient = $this->entityManager->getRepository(Customer::class)->findOneBy(['idCustomer' => $cli["id_customer"]]);

            if (!is_null($existingClient)) {
                $client = $existingClient;
            } else {
                $client = new Customer();
                $client->setDateAdd(new \DateTime($cli["date_add"]));
            }
            $client->setNom($cli["lastname"]);
            $client->setPrenom($cli["firstname"]);
            $client->setEmail($cli["email"]);
            $client->setIdCustomer($cli["id_customer"]);
            $client->setDateUpdate(new \DateTime($cli["date_upd"]));
            $client->setTelephone((!empty($cli["phone"])) ? $cli["phone"] : $cli["phone_mobile"]);
            if (!is_null($cli["zip"]) && $cli["zip"] != '')
            {
                $client->setAddress($cli["address1"] . ' ' . $cli["address2"]);
                $client->setZip($cli["zip"]);
                $client->setCity($cli["city"]);
                $client->setCountry($cli["country"]);
            }

            $this->entityManager->persist($client);
            $this->entityManager->flush();

            $clients[$client->getIdCustomer()] = $client;
        }

        // Gestion des commandes
        $query = $pdo->query("
                SELECT 
                       o.invoice_date as date_facture,
                       o.reference as num_commande,
                       concat('#FA', IF(LENGTH(o.invoice_number) < 6, LPAD(o.invoice_number,6,'0') ,o.invoice_number)) as num_facture,
                       posl.name as statut_commande,
                       o.payment as mode_paiement,
                       o.total_products as total_ht_hors_livraison,
                       o.total_shipping as frais_de_livraison,
                       o.total_paid as total_TTC,
                       o.total_paid_tax_excl as total_HT,
                       c.email,
                       c.id_customer
                FROM `ps_orders` o
                JOIN ps_customer c on c.id_customer = o.id_customer
                JOIN ps_order_state_lang posl ON posl.id_order_state=o.current_state
                WHERE posl.id_lang=1 and o.invoice_date != '0000-00-00 00:00:00' and o.invoice_number != 0
        ");
        $resultatsCommandesPS = $query->fetchAll();

        foreach($resultatsCommandesPS as $com) {
            $existingCommande = $this->entityManager->getRepository(Commande::class)->findOneBy(['numC' => $com["num_commande"]]);
            if (is_null($existingCommande)) {
                $commande = new Commande();
                $commande->setDateF(new \DateTime($com["date_facture"]));
                $commande->setNumC($com["num_commande"]);
                $commande->setNumF($com["num_facture"]);
                $commande->setEmail($com["email"]);
            } else {
                $commande = $existingCommande;
            }
            $commande->setStatut($com["statut_commande"]);
            $commande->setModePay($com["mode_paiement"]);
            $commande->setTotalHtHorsLivraison($com["total_ht_hors_livraison"]);
            $commande->setTotalHt($com["total_HT"]);
            $commande->setTotalFraisLivraison($com["frais_de_livraison"]);
            $commande->setTotalTtc($com["total_TTC"]);
            $commande->setCustomer($clients[$com["id_customer"]]);

            $this->entityManager->persist($commande);
            $this->entityManager->flush();
        }

        // Gestion des CA et quantités par articles
        $query = $pdo->query("
                SELECT  a.date_add as date_ajout,
                        b.id_order_detail as id_order_detail,
                        b.product_reference as ref, 
                        b.product_name as nom, 
                        b.product_quantity as qty, 
                        b.product_price as prix
                FROM ps_orders AS a
                INNER JOIN ps_order_detail AS b ON b.id_order = a.id_order
                INNER JOIN ps_order_invoice AS c ON c.id_order = b.id_order 
        ");
        $resultatsArticlesPS = $query->fetchAll();

        foreach ($resultatsArticlesPS as $art)
        {
            $existingArticle = $this->entityManager->getRepository(Article::class)->findOneBy(['idOrderDetail' => $art["id_order_detail"]]);
            if (is_null($existingArticle)) {
                $existingArticle = new Article();
                $existingArticle->setIdOrderDetail($art["id_order_detail"]);
            }
            $existingArticle->setDateAdd(new \DateTime($art["date_ajout"]));
            $existingArticle->setReference($art["ref"]);
            $existingArticle->setNom($art["nom"]);
            $existingArticle->setQty($art["qty"]);
            $existingArticle->setPrix($art["prix"]);

            $this->entityManager->persist($existingArticle);
            $this->entityManager->flush();
        }

        return 1;
    }
}