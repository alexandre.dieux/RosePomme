<?php

namespace App\Command;

use App\Entity\Commande;
use App\Entity\Customer;
use App\Traits\ExcelTrait;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class GenerateCustomerExcel extends Command
{
    use ExcelTrait;
    private $entityManager;
    private $mailer;

    public function __construct(EntityManagerInterface $entityManager, MailerInterface $mailer)
    {
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;

        parent::__construct();
    }

    protected function configure(): void
    {
        // On set le nom de la commande
        $this->setName('ps:customer');
        // On set la description
        $this->setDescription("Permet de générer un excel des mails clients");
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $clients = $this->entityManager->getRepository(Customer::class)->findAll();

        $datas = $this->formatClient($clients);

        $columns = ["email"];

        $dateStart = new \DateTime();
        $weekNumber =  $dateStart->format('W');
        $mailObject = 'Excel Client';
        $mailBody = '<p>Bonjour,<br />vous trouverez ci-joint le tableau des mails client</p>';
        $fileName = 'excelClient-S'.$weekNumber.'.xlsx';

        $this->generateExcelGeneric($columns, $datas, $mailObject, $mailBody, $fileName, $this->isDebug);

        return 1;
    }

}