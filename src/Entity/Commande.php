<?php

namespace App\Entity;

use App\Repository\CommandeRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CommandeRepository::class)]
class Commande
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateC = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateF = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $numC = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $numF = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $statut = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $modePay = null;

    #[ORM\Column(nullable: true)]
    private ?float $totalHt = null;

    #[ORM\Column(nullable: true)]
    private ?float $totalHtHorsLivraison = null;

    #[ORM\Column(nullable: true)]
    private ?float $totalFraisLivraison = null;

    #[ORM\Column(nullable: true)]
    private ?float $totalTtc = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $email = null;

    #[ORM\ManyToOne(inversedBy: 'commandes')]
    private ?Customer $customer = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateC(): ?\DateTimeInterface
    {
        return $this->dateC;
    }

    public function setDateC(?\DateTimeInterface $dateC): static
    {
        $this->dateC = $dateC;

        return $this;
    }

    public function getDateF(): ?\DateTimeInterface
    {
        return $this->dateF;
    }

    public function setDateF(?\DateTimeInterface $dateF): static
    {
        $this->dateF = $dateF;

        return $this;
    }

    public function getNumC(): ?string
    {
        return $this->numC;
    }

    public function setNumC(?string $numC): static
    {
        $this->numC = $numC;

        return $this;
    }

    public function getNumF(): ?string
    {
        return $this->numF;
    }

    public function setNumF(?string $numF): static
    {
        $this->numF = $numF;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(?string $statut): static
    {
        $this->statut = $statut;

        return $this;
    }

    public function getModePay(): ?string
    {
        return $this->modePay;
    }

    public function setModePay(?string $modePay): static
    {
        $this->modePay = $modePay;

        return $this;
    }

    public function getTotalHt(): ?float
    {
        return $this->totalHt;
    }

    public function setTotalHt(?float $totalHt): static
    {
        $this->totalHt = $totalHt;

        return $this;
    }

    public function getTotalHtHorsLivraison(): ?float
    {
        return $this->totalHtHorsLivraison;
    }

    public function setTotalHtHorsLivraison(?float $totalHtHorsLivraison): static
    {
        $this->totalHtHorsLivraison = $totalHtHorsLivraison;

        return $this;
    }

    public function getTotalFraisLivraison(): ?float
    {
        return $this->totalFraisLivraison;
    }

    public function setTotalFraisLivraison(?float $totalFraisLivraison): static
    {
        $this->totalFraisLivraison = $totalFraisLivraison;

        return $this;
    }

    public function getTotalTtc(): ?float
    {
        return $this->totalTtc;
    }

    public function setTotalTtc(?float $totalTtc): static
    {
        $this->totalTtc = $totalTtc;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): static
    {
        $this->customer = $customer;

        return $this;
    }
}
