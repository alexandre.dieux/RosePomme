<?php

namespace App\Traits;

use App\Entity\Commande;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

trait ExcelTrait
{
    use ConfigTrait;

    private $entityManager;
    private $mailer;

    public function __construct(EntityManagerInterface $entityManager, MailerInterface $mailer)
    {
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
    }

    public function generateContent($dateStart, $dateEnd) {
        $commandeRepository = $this->entityManager->getRepository(Commande::class);

        $criteria = new Criteria();
        $criteria
            ->where(Criteria::expr()->neq('statut', 'Annulé'))
            ->andWhere(Criteria::expr()->gte('dateF', $dateStart))
            ->andWhere(Criteria::expr()->lte('dateF', $dateEnd));

        $commandes = $commandeRepository->matching($criteria);

        $tabData = [];
        $tabTotalByStatut = [];

        $totalHTHorsFraisLivraison = 0;
        $totalFraisLivraison = 0;
        $totalTTC = 0;
        $totalRemboursement = 0;
        foreach($commandes as $c) {
            $totalHTHorsFraisLivraison += $c->getTotalHtHorsLivraison();
            $totalFraisLivraison += $c->getTotalFraisLivraison();
            $totalTTC += $c->getTotalTtc();
            // TODO Gestion Remboursement
            $totalRemboursement += 0;

            if (!array_key_exists($c->getStatut(), $tabTotalByStatut)) {
                $tabTotalByStatut[$c->getStatut()] = [
                    "HTHorsFraisLivraison" => 0,
                    "FraisLivraison" => 0,
                    "TTC" => 0,
                    "Remboursement" => 0,
                ];
            }

            $tabTotalByStatut[$c->getStatut()]["HTHorsFraisLivraison"] += $c->getTotalHtHorsLivraison();
            $tabTotalByStatut[$c->getStatut()]["FraisLivraison"] += $c->getTotalFraisLivraison();
            $tabTotalByStatut[$c->getStatut()]["TTC"] += $c->getTotalTtc();
            $tabTotalByStatut[$c->getStatut()]["Remboursement"] += 0;

            $tabData[] = [
                $c->getDateF()->format('d/m/Y H:i:s'),
                $c->getNumC(),
                $c->getNumF(),
                $c->getStatut(),
                $c->getModePay(),
                $c->getTotalHtHorsLivraison(),
                $c->getTotalFraisLivraison(),
                $c->getTotalTtc(),
                '',
                $c->getEmail()
            ];
        }

        $tabData[] = [
            '',
            '',
            '',
            '',
            'TOTAL',
            $totalHTHorsFraisLivraison,
            $totalFraisLivraison,
            $totalTTC,
            $totalRemboursement,
            ''
        ];

        return [
            "datas" => $tabData,
            "datasByStatut" => $tabTotalByStatut
        ];
    }

    public function generateExcelCommande($data, $dataByStatus, $mailObject, $mailBody, $nomFichier, $debug = false): void
    {
        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        // Set column names
        $columnNames = [
            'Date de la facture',
            'Numero de commande',
            'Numero de facture',
            'Statut de la commande',
            'Mode de paiement',
            'Total HT hors frais de livraison',
            'Frais de livraison',
            'Total TTC',
            'Remboursement',
            'Email client'
        ];

        // Set column names
        $columnNamesByStatus = [
            'Statut des commandes',
            'Total HT hors frais de livraison',
            'Frais de livraison',
            'Total TTC',
            'Remboursement',
        ];

        //dump($samples);
        $styleEnTete = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'left' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'bottom' => [
                    'borderStyle' => Border::BORDER_THICK,
                ]
            ]
        ];

        $styleContenu = [
            'borders' => [
                'top' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'left' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'bottom' => [
                    'borderStyle' => Border::BORDER_THIN,
                ]
            ]
        ];

        $styleTotal = [
            'font' => [
                'bold' => true,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => Border::BORDER_THICK,
                ],
                'left' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'bottom' => [
                    'borderStyle' => Border::BORDER_THIN,
                ]
            ]
        ];

        // Gestion des en-têtes
        $columnLetter = 'A';
        $ligne=1;
        foreach ($columnNames as $columnName) {
            // Allow to access AA column if needed and more
            $sheet->setCellValue($columnLetter.$ligne, strtoupper($columnName));
            $sheet->getStyle($columnLetter.$ligne)->applyFromArray($styleEnTete);
            $columnLetter++;
        }

        // Gestion du contenu
        foreach($data as $index => $ligneCommande) {
            $ligne++;
            $columnLetter = 'A';
            foreach($ligneCommande as $value) {
                $sheet->setCellValue($columnLetter.$ligne, $value);
                if ($index != sizeof($data)-1) {
                    $sheet->getStyle($columnLetter.$ligne)->applyFromArray($styleContenu);
                }
                if ($index == sizeof($data)-1 && $columnLetter >= 'E' && $columnLetter <= 'I') {
                    $sheet->getStyle($columnLetter.$ligne)->applyFromArray($styleTotal);
                }
                $columnLetter++;
            }
        }

        // Gestion des en-têtes
        $columnLetter = 'A';
        $ligne=$ligne+2;
        foreach ($columnNamesByStatus as $columnName) {
            // Allow to access AA column if needed and more
            $sheet->setCellValue($columnLetter.$ligne, strtoupper($columnName));
            $sheet->getStyle($columnLetter.$ligne)->applyFromArray($styleEnTete);
            $columnLetter++;
        }

        // Gestion du contenu
        foreach($dataByStatus as $status => $data) {
            $ligne++;
            $sheet->setCellValue('A'.$ligne, $status);
            $sheet->getStyle('A'.$ligne)->applyFromArray($styleContenu);
            $sheet->setCellValue('B'.$ligne, $data["HTHorsFraisLivraison"]);
            $sheet->getStyle('B'.$ligne)->applyFromArray($styleContenu);
            $sheet->setCellValue('C'.$ligne, $data["FraisLivraison"]);
            $sheet->getStyle('C'.$ligne)->applyFromArray($styleContenu);
            $sheet->setCellValue('D'.$ligne, $data["TTC"]);
            $sheet->getStyle('D'.$ligne)->applyFromArray($styleContenu);
            $sheet->setCellValue('E'.$ligne, $data["Remboursement"]);
            $sheet->getStyle('E'.$ligne)->applyFromArray($styleContenu);
        }


        $columnLetter--;

        foreach(range('A',$columnLetter) as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $filename = $nomFichier;

        ob_start();
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
        $result = ob_get_clean();

        if (!$debug) {
            $this->sendMail('serviceclient@rosepomme.fr', 'serviceclient@rosepomme.fr', $mailObject, $mailBody, $result, $filename);
        }
        $this->sendMail('serviceclient@rosepomme.fr', 'dieux.alexandre@gmail.com', $mailObject, $mailBody, $result, $filename);
    }

    public function generateExcelGeneric($columns, $data, $mailObject, $mailBody, $nomFichier, $debug = false): void
    {
        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        // Génération générique
        $styleEnTete = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'left' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'bottom' => [
                    'borderStyle' => Border::BORDER_THICK,
                ]
            ]
        ];

        $styleContenu = [
            'borders' => [
                'top' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'left' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'bottom' => [
                    'borderStyle' => Border::BORDER_THIN,
                ]
            ]
        ];

        $styleTotal = [
            'font' => [
                'bold' => true,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => Border::BORDER_THICK,
                ],
                'left' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'bottom' => [
                    'borderStyle' => Border::BORDER_THIN,
                ]
            ]
        ];

        // Gestion des en-têtes
        $columnLetter = 'A';
        $ligne=1;
        foreach ($columns as $columnName) {
            // Allow to access AA column if needed and more
            $sheet->setCellValue($columnLetter.$ligne, strtoupper($columnName));
            $sheet->getStyle($columnLetter.$ligne)->applyFromArray($styleEnTete);
            $columnLetter++;
        }

        // Gestion du contenu
        foreach($data as $index => $ligneValeurs) {
            $ligne++;
            $columnLetter = 'A';
            foreach($ligneValeurs as $value) {
                $sheet->setCellValue($columnLetter.$ligne, $value);
                $sheet->getStyle($columnLetter.$ligne)->applyFromArray($styleContenu);
                $columnLetter++;
            }
        }

        $columnLetter--;

        foreach(range('A',$columnLetter) as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $filename = $nomFichier;

        ob_start();
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
        $result = ob_get_clean();

        if (!$debug) {
            $this->sendMail('serviceclient@rosepomme.fr', 'serviceclient@rosepomme.fr', $mailObject, $mailBody, $result, $filename);
        }
        $this->sendMail('serviceclient@rosepomme.fr', 'dieux.alexandre@gmail.com', $mailObject, $mailBody, $result, $filename);
    }

    public function sendMail($from, $to, $subject, $body, $attachment, $filename): void
    {
        // Envoi du mail
        $email = (new Email())
            ->from($from)
            ->to($to)
            ->subject($subject)
            ->html($body)
            ->attach($attachment, $filename);

        $this->mailer->send($email);
    }

    public function formatClient($clients): array
    {
        $datas = [];
        $datasUnformatted = [];

        foreach ($clients as $client) {
            $formattedMail = trim(strtolower($client->getEmail()));
            if (!in_array($formattedMail, $datasUnformatted) && $formattedMail != 'email@email.com') {
                $datasUnformatted[] = $formattedMail;
                $datas[] = [$formattedMail];
            }
        }

        return $datas;
    }

    public function formatClientFull($clients): array
    {
        $datas = [];
        $datasUnformatted = [];

        foreach ($clients as $client) {
            $formattedMail = trim(strtolower($client->getEmail()));
            if (!in_array($formattedMail, $datasUnformatted) && $formattedMail != 'email@email.com' & !is_null($client->getZip())) {
                $datasUnformatted[] = $formattedMail;
                $datas[] = [
                    $formattedMail,
                    $client->getNom(),
                    $client->getPrenom(),
                    $client->getTelephone(),
                    $client->getAddress(),
                    $client->getZip(),
                    $client->getCity(),
                    $client->getCountry()
                ];
            }
        }

        return $datas;
    }

    public function formatCustomerPhone($clients): array
    {
        $datas = [];
        $datasUnformatted = [];

        foreach ($clients as $client) {
            $tel = $client->getTelephone();
            if (!in_array($tel, $datasUnformatted) && !empty($tel)) {
                $datasUnformatted[] = $tel;
                $datas[] = [$tel];
            }
        }

        return $datas;
    }

    public function formatArticles($articles): array
    {
        $datasByRef = [];
        $datas = [];

        foreach ($articles as $article) {
            if (!array_key_exists($article->getReference(), $datasByRef)) {
                $arrayNomProduit = explode('(', $article->getNom());
                $nomProduit = $arrayNomProduit[0];
                $declinaisonProduit = '';
                if (sizeof($arrayNomProduit) > 1) {
                    $declinaisonProduit = $arrayNomProduit[1];
                }
                $datasByRef[$article->getReference()] = [
                    'reference' => $article->getReference(),
                    'nom' => trim($nomProduit),
                    'declinaison' => trim(str_replace(')', '', $declinaisonProduit)),
                    'qty' => (float) $article->getQty(),
                    'ca' => (float) $article->getPrix()
                ];
            } else {
                $datasByRef[$article->getReference()]['qty'] += (float) $article->getQty();
                $datasByRef[$article->getReference()]['ca'] += (float) $article->getPrix();
            }
        }

        foreach ($datasByRef as $reference => $data) {
            $datas[] = [
                $data['reference'],
                $data['nom'],
                $data['declinaison'],
                $data['qty'],
                round((float) $data['ca'], 2)
            ];
        }

        return $datas;
    }


    private function generateExcelTotalCommande(\DateTime $dateStart, \DateTime $dateEnd, string $mailObject, string $mailBody, string $filename, bool $debug)
    {
        $pdo = new \PDO('mysql:host='.$this->IMPORTHOST.';dbname='.$this->IMPORTDB, $this->IMPORTUSER, $this->IMPORTPWD);

        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('COMMANDES HORS AVOIR');

        //dump($samples);
        $styleEnTete = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'left' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'bottom' => [
                    'borderStyle' => Border::BORDER_THICK,
                ]
            ]
        ];

        $styleContenu = [
            'borders' => [
                'top' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'left' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'bottom' => [
                    'borderStyle' => Border::BORDER_THIN,
                ]
            ]
        ];

        $styleTotal = [
            'font' => [
                'bold' => true,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => Border::BORDER_THICK,
                ],
                'left' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'bottom' => [
                    'borderStyle' => Border::BORDER_THIN,
                ]
            ]
        ];

        /***************************************************************************************************************
         ***************************************************************************************************************
         * GESTION DES COMMANDES
         ***************************************************************************************************************
         **************************************************************************************************************/
        $query = $pdo->query("
                SELECT 
                       o.invoice_date as date_facture,
                       o.reference as num_commande,
                       concat('#FA', IF(LENGTH(o.invoice_number) < 6, LPAD(o.invoice_number,6,'0') ,o.invoice_number)) as num_facture,
                       posl.name as statut_commande,
                       o.payment as mode_paiement,
                       o.total_products as total_ht_hors_livraison,
                       o.total_shipping as frais_de_livraison,
                       o.total_paid as total_TTC,
                       o.total_paid_tax_excl as total_HT,
                       c.email,
                       c.id_customer,
                       o.id_order as id
                FROM `ps_orders` o
                JOIN ps_customer c on c.id_customer = o.id_customer
                JOIN ps_order_state_lang posl ON posl.id_order_state=o.current_state
                WHERE posl.id_lang=1 and o.invoice_date != '0000-00-00 00:00:00' and o.invoice_number != 0
                AND o.invoice_date >= '" .$dateStart->format('Y-m-d H:i:s'). "'
                ORDER BY o.invoice_date ASC 
        ");
        $resultatsCommandesPS = $query->fetchAll();

        // Gestion des en-têtes
        // Set column names
        $columnNames = [
            'ID',
            'id_client',
            'Date de la facture',
            'Numero de commande',
            'Numero de facture',
            'Statut de la commande',
            'Mode de paiement',
            'Total HT hors frais de livraison',
            'Frais de livraison',
            'Total TTC',
            'Email client'
        ];
        $columnLetter = 'A';
        $ligne=1;
        foreach ($columnNames as $columnName) {
            // Allow to access AA column if needed and more
            $sheet->setCellValue($columnLetter.$ligne, strtoupper($columnName));
            $sheet->getStyle($columnLetter.$ligne)->applyFromArray($styleEnTete);
            $columnLetter++;
        }

        // Gestion du contenu
        foreach($resultatsCommandesPS as $index => $ligneCommande) {
            $ligne++;
            $columnLetter = 'A';
            $sheet->setCellValue($columnLetter.$ligne, $ligneCommande['id']);
            $sheet->getStyle($columnLetter.$ligne)->applyFromArray($styleContenu);
            $columnLetter++;

            $sheet->setCellValue($columnLetter.$ligne, $ligneCommande['id_customer']);
            $sheet->getStyle($columnLetter.$ligne)->applyFromArray($styleContenu);
            $columnLetter++;

            $sheet->setCellValue($columnLetter.$ligne, $ligneCommande['date_facture']);
            $sheet->getStyle($columnLetter.$ligne)->applyFromArray($styleContenu);
            $columnLetter++;

            $sheet->setCellValue($columnLetter.$ligne, $ligneCommande['num_commande']);
            $sheet->getStyle($columnLetter.$ligne)->applyFromArray($styleContenu);
            $columnLetter++;

            $sheet->setCellValue($columnLetter.$ligne, $ligneCommande['num_facture']);
            $sheet->getStyle($columnLetter.$ligne)->applyFromArray($styleContenu);
            $columnLetter++;

            $sheet->setCellValue($columnLetter.$ligne, $ligneCommande['statut_commande']);
            $sheet->getStyle($columnLetter.$ligne)->applyFromArray($styleContenu);
            $columnLetter++;

            $sheet->setCellValue($columnLetter.$ligne, $ligneCommande['mode_paiement']);
            $sheet->getStyle($columnLetter.$ligne)->applyFromArray($styleContenu);
            $columnLetter++;

            $sheet->setCellValue($columnLetter.$ligne, $ligneCommande['total_ht_hors_livraison']);
            $sheet->getStyle($columnLetter.$ligne)->applyFromArray($styleContenu);
            $columnLetter++;

            $sheet->setCellValue($columnLetter.$ligne, $ligneCommande['frais_de_livraison']);
            $sheet->getStyle($columnLetter.$ligne)->applyFromArray($styleContenu);
            $columnLetter++;

            $sheet->setCellValue($columnLetter.$ligne, $ligneCommande['total_TTC']);
            $sheet->getStyle($columnLetter.$ligne)->applyFromArray($styleContenu);
            $columnLetter++;

            $sheet->setCellValue($columnLetter.$ligne, $ligneCommande['email']);
            $sheet->getStyle($columnLetter.$ligne)->applyFromArray($styleContenu);
        }

        $columnLetter--;

        foreach(range('A',$columnLetter) as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        /***************************************************************************************************************
         ***************************************************************************************************************
         * FIN GESTION DES COMMANDES
         ***************************************************************************************************************
         **************************************************************************************************************/


        /***************************************************************************************************************
         ***************************************************************************************************************
         * GESTION DES LIGNES COMMANDES
         ***************************************************************************************************************
         **************************************************************************************************************/
        $sheet = $spreadsheet->createSheet();
        $sheet->setTitle('LIGNES COMMANDES');

        $query = $pdo->query("
                SELECT  				
                        a.date_add as date_ajout,
						b.id_order as id_commande,
                        b.id_order_detail as id_order_detail,
                        b.product_id as id_produit,
                        b.product_reference as ref,
                        b.product_name as nom,
                        b.product_quantity as qty,
                        b.product_price as prix,
                        b.product_quantity_refunded as qty_rembourse,
                        b.reduction_percent as pourcentage_reduc
                FROM ps_order_detail AS b
                INNER JOIN ps_orders AS a ON b.id_order = a.id_order
                INNER JOIN ps_order_invoice AS c ON c.id_order = b.id_order
                WHERE a.invoice_date >= '" .$dateStart->format('Y-m-d H:i:s'). "'
                ORDER BY a.invoice_date ASC 
        ");
        $resultatsCommandesPS = $query->fetchAll();

        // Gestion des en-têtes
        // Set column names
        $columnNames = [
            'id_ligne_commande',
            'id_commande',
            'id_produit',
            'Date',
            'reference',
            'nom produit',
            'déclinaison',
            'quantité commandée',
            'prix payé HT par le client',
            'quantité remboursée',
            'Pourcentage de réduction'
        ];
        $columnLetter = 'A';
        $ligne=1;
        foreach ($columnNames as $columnName) {
            // Allow to access AA column if needed and more
            $sheet->setCellValue($columnLetter.$ligne, strtoupper($columnName));
            $sheet->getStyle($columnLetter.$ligne)->applyFromArray($styleEnTete);
            $columnLetter++;
        }

        // Gestion du contenu
        foreach($resultatsCommandesPS as $index => $ligneCommande) {
            $ligne++;
            $columnLetter = 'A';
            $sheet->setCellValue($columnLetter . $ligne, $ligneCommande['id_order_detail']);
            $sheet->getStyle($columnLetter . $ligne)->applyFromArray($styleContenu);
            $columnLetter++;

            $sheet->setCellValue($columnLetter . $ligne, $ligneCommande['id_commande']);
            $sheet->getStyle($columnLetter . $ligne)->applyFromArray($styleContenu);
            $columnLetter++;

            $sheet->setCellValue($columnLetter . $ligne, $ligneCommande['id_produit']);
            $sheet->getStyle($columnLetter . $ligne)->applyFromArray($styleContenu);
            $columnLetter++;

            $sheet->setCellValue($columnLetter . $ligne, $ligneCommande['date_ajout']);
            $sheet->getStyle($columnLetter . $ligne)->applyFromArray($styleContenu);
            $columnLetter++;

            $sheet->setCellValue($columnLetter . $ligne, $ligneCommande['ref']);
            $sheet->getStyle($columnLetter . $ligne)->applyFromArray($styleContenu);
            $columnLetter++;

            $arrayNomProduit = explode('(', $ligneCommande['nom']);
            $nomProduit = $arrayNomProduit[0];
            $declinaisonProduit = '';
            if (sizeof($arrayNomProduit) > 1) {
                $declinaisonProduit = $arrayNomProduit[1];
            }
            $sheet->setCellValue($columnLetter . $ligne, trim($nomProduit));
            $sheet->getStyle($columnLetter . $ligne)->applyFromArray($styleContenu);
            $columnLetter++;

            $sheet->setCellValue($columnLetter . $ligne, trim(str_replace(')', '', $declinaisonProduit)));
            $sheet->getStyle($columnLetter . $ligne)->applyFromArray($styleContenu);
            $columnLetter++;

            $sheet->setCellValue($columnLetter . $ligne, $ligneCommande['qty']);
            $sheet->getStyle($columnLetter . $ligne)->applyFromArray($styleContenu);
            $columnLetter++;

            $sheet->setCellValue($columnLetter . $ligne, round($ligneCommande['prix'], 2));
            $sheet->getStyle($columnLetter . $ligne)->applyFromArray($styleContenu);
            $columnLetter++;

            $sheet->setCellValue($columnLetter . $ligne, $ligneCommande['qty_rembourse']);
            $sheet->getStyle($columnLetter . $ligne)->applyFromArray($styleContenu);
            $columnLetter++;

            $sheet->setCellValue($columnLetter . $ligne, $ligneCommande['pourcentage_reduc']);
            $sheet->getStyle($columnLetter . $ligne)->applyFromArray($styleContenu);
            $columnLetter++;
        }

        $columnLetter--;

        foreach(range('A',$columnLetter) as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        /***************************************************************************************************************
         ***************************************************************************************************************
         * FIN GESTION DES LIGNES COMMANDES
         ***************************************************************************************************************
         **************************************************************************************************************/

        /***************************************************************************************************************
         ***************************************************************************************************************
         * GESTION DES PRODUITS
         ***************************************************************************************************************
         **************************************************************************************************************/
        $sheet = $spreadsheet->createSheet();
        $sheet->setTitle('ARTICLES');

        $query = $pdo->query("
                SELECT
                       a.`id_product`, 
                       b.`name` AS `name`, 
                       `reference`, 
                       a.`price` AS `price`, 
                       sa.`active` AS `active`,  
                       a.`id_shop_default`, 
                       cl.`name` AS `name_category`, 
                       sa.`price`, 0 AS `price_final`,
                       sav.`quantity` AS `sav_quantity` 
                FROM `ps_product` a 
                LEFT JOIN `ps_product_lang` b ON (b.`id_product` = a.`id_product` AND b.`id_lang` = 1 AND b.`id_shop` = 1)
                LEFT JOIN `ps_stock_available` sav ON (sav.`id_product` = a.`id_product` 
                          AND sav.`id_product_attribute` = 0
                          AND sav.id_shop = 1  
                          AND sav.id_shop_group = 0 )
                JOIN `ps_product_shop` sa ON (a.`id_product` = sa.`id_product` AND sa.id_shop = a.id_shop_default)
                LEFT JOIN `ps_category_lang` cl ON (sa.`id_category_default` = cl.`id_category` AND b.`id_lang` = cl.`id_lang` AND cl.id_shop = a.id_shop_default)
                LEFT JOIN `ps_shop` shop ON (shop.id_shop = a.id_shop_default)
                WHERE 1  
                ORDER BY `a`.`id_product`  ASC
        ");
        $resultatsCommandesPS = $query->fetchAll();

        // Gestion des en-têtes
        // Set column names
        $columnNames = [
            'id_produit',
            'nom',
            'reference',
            'prix HT',
            'active'
        ];
        $columnLetter = 'A';
        $ligne=1;
        foreach ($columnNames as $columnName) {
            // Allow to access AA column if needed and more
            $sheet->setCellValue($columnLetter.$ligne, strtoupper($columnName));
            $sheet->getStyle($columnLetter.$ligne)->applyFromArray($styleEnTete);
            $columnLetter++;
        }

        // Gestion du contenu
        foreach($resultatsCommandesPS as $index => $ligneCommande) {
            $ligne++;
            $columnLetter = 'A';
            $sheet->setCellValue($columnLetter . $ligne, $ligneCommande['id_product']);
            $sheet->getStyle($columnLetter . $ligne)->applyFromArray($styleContenu);
            $columnLetter++;

            $sheet->setCellValue($columnLetter . $ligne, $ligneCommande['name']);
            $sheet->getStyle($columnLetter . $ligne)->applyFromArray($styleContenu);
            $columnLetter++;

            $sheet->setCellValue($columnLetter . $ligne, $ligneCommande['reference']);
            $sheet->getStyle($columnLetter . $ligne)->applyFromArray($styleContenu);
            $columnLetter++;

            $sheet->setCellValue($columnLetter . $ligne, round($ligneCommande['price'], 2));
            $sheet->getStyle($columnLetter . $ligne)->applyFromArray($styleContenu);
            $columnLetter++;

            $sheet->setCellValue($columnLetter . $ligne, $ligneCommande['active']);
            $sheet->getStyle($columnLetter . $ligne)->applyFromArray($styleContenu);
            $columnLetter++;
        }

        $columnLetter--;

        foreach(range('A',$columnLetter) as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        /***************************************************************************************************************
         ***************************************************************************************************************
         * FIN GESTION DES PRODUITS
         ***************************************************************************************************************
         **************************************************************************************************************/

        ob_start();
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
        $result = ob_get_clean();

        if (!$debug) {
            // $this->sendMail('serviceclient@rosepomme.fr', 'serviceclient@rosepomme.fr', $mailObject, $mailBody, $result, $filename);
            $this->sendMail('serviceclient@rosepomme.fr', 'marc@rosepomme.com', $mailObject, $mailBody, $result, $filename);
        }
        $this->sendMail('serviceclient@rosepomme.fr', 'dieux.alexandre@gmail.com', $mailObject, $mailBody, $result, $filename);
    }
}
